package antipatterns;

public class DataClassWithEffectivelyFinalFields {

  private final String name; // should be final
  private final int number; // should be final

  public DataClassWithEffectivelyFinalFields(final String name, final int number) {
    this.name = name;
    this.number = number;
  }

  public String getName() {
    return name;
  }

  public int getNumber() {
    return number;
  }
}
