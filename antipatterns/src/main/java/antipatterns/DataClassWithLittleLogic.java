package antipatterns;

// ok, but see the test
public class DataClassWithLittleLogic {

  private String name;
  private int number;

  public DataClassWithLittleLogic(final String name, final int number) {
    this.name = name;
    this.number = number;
  }

  @Override
  public String toString() {
    return String.format("[DataClassWithLittleLogic name=%s number=%s]", name, number);
  }

  public DataClassWithLittleLogic() {
  }

  public String getName() {
    return name;
  }
  public void setName(final String name) {
    this.name = name;
  }

  public int getNumber() {
    return number;
  }
  public void setNumber(final int number) {
    this.number = number;
  }
}
