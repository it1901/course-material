package antipatterns;

import java.util.Collection;
import java.util.Iterator;

public class NonEncapsulatedList implements Iterable<String> {

  private final Collection<String> names;

  public NonEncapsulatedList(final Collection<String> names) {
    this.names = names;
  }

  public Collection<String> getNames() {
    return names;
  }

  public Iterator<String> iterator() {
    return names.iterator();
  }
}
