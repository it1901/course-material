package antipatterns;

// see DataClassWithValidationTest
public class DataClassWithValidation {

  private String name;

  public DataClassWithValidation(final String name) {
    // should use setName to ensure validation logic is run
    this.name = name;
  }

  public DataClassWithValidation() {
  }

  public String getName() {
    return name;
  }
  public void setName(final String name) {
    if (name.startsWith(" ")) {
      // should rather use trim() or strip() and check the length
      throw new IllegalArgumentException("Name cannot begin with blank");
    }
    this.name = name;
  }
}
