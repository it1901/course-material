package antipatterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DataClassWithValidationTest {

  @Test
  public void testSetName() {
    try {
      new DataClassWithValidation(" name starting with blank");
      Assertions.fail("Should throw IllegalArgumentException");
    } catch (final IllegalArgumentException iae) {
    } catch (final Exception iae) {
      Assertions.fail("Should throw IllegalArgumentException");
    }

    try {
      new DataClassWithValidation("ok name");
    } catch (final Exception iae) {
      Assertions.fail();
    }
  }

  @Test
  public void testBetterSetName_invalidName() {
    Assertions.assertThrows(IllegalArgumentException.class, () -> new DataClassWithValidation(" name starting with blank"));
  }

  @Test
  public void testBetterSetName_validName() {
    new DataClassWithValidation("ok name");
  }
}
