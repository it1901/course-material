package antipatterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DataClassWithLittleLogicTest {

  private DataClassWithLittleLogic instance1, instance2;

  @BeforeEach
  public void setup() {
    instance1 = new DataClassWithLittleLogic();
    instance2 = new DataClassWithLittleLogic("High", 5);
  }

  @Test
  public void testConstructor() {
    Assertions.assertEquals("High", instance2.getName());
  }

  @Test
  public void testToString() {
    Assertions.assertEquals("[DataClassWithLittleLogic name=High number=5]", instance2.toString());
  }

  @Test
  public void testSetName() {
    instance1.setName("Low");
    Assertions.assertEquals("Low", instance1.getName());
  }
}
