= Build tools
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Build tools
:LECTURE_NO: 4th lecture

include::{includedir}header.adoc[]


[.smaller-80][.center-paragraph]
IT1901 Fall 2024 - {LECTURE_NO}

== Agenda

[%step]
- Administrative issues
- 1st Group Exercise
- Build tools
- More on development environment
** VSCode extensions
** Eclipse Che
- Agile Practices
- Summary


[background-color = "#124990"]
[color = "#fff6d5"]
== Administrative issues

== Groups

- groups have been established
- each student team got a BlackBoard group and a GitLab group 
- the GitLab group contains the "standard" repository
- check if you are properly assigned to your group



== Approaching Deadlines

- Torsdag, 12. september / 23:59 
** 1st individual deliverable

- Torsdag, 19. september / 23:59 
** 1st group deliverable

- Torsdag, 19. september / 23:59 
** group contract

 
== Getting help

- ask your group TA
- Q and A sessions every Thursday starting week 37
- some lectures will also Q and A sections 
- technical help - helpdesk on teams starting next week


== ! 

image::../images/lecture04/overview.png[canvas, size=contain]


[background-color = "#124990"]
[color = "#fff6d5"]
== 1st Group Exercise

== Start group work

 - finish and deliver the group contract
 - discuss and select domain / aplication for group project
 - discuss and agree on workflow, meetings etc
 - set up your gitlab workspace and repository
 - plan the sprint  / iteration for the first deliverable

== About domain selection

- choose an app / service that you know well and select a couple of features to implement during the semester
- point is to learn by implementing these using the required architectures
- we are not looking for quantity but for quality, so just few core features will suffice

== About domain selection (2)

- the chosen app needs to be suitable for a cloud based service
- there must therefore be some dynamic data per user managed by the server. 
- eg. a (currency) calculation will NOT fit such a project. 
- one good starting point are the cases from the HCI course (MMI)

== 3 step release plan

- Minimal application
- Application with more features / improved quality 
- Final technical – client server / REST API / more features / alternative client

== Minimal application

- a simple app is created with JavaFX and FXML, with relatively minimal functionality 
- can save data to a file and start up again where it left off
- divided into separate modules
- good point to start could be the modules-template (from the individual exercise)
 ** one can also use the todo list example as inspiration

== Requirements (1)

 - project must be in the group repo in gitlab
 - documentation for each release is placed in a separate folder
 ** 1st deliverable (release)-  `...groups-2024/gr24nn/gr24nn/docs/release1`
 - a milestone in Gitlab should refer to the corresponding documentation and issues

== Requirements (2)

- project must build with Maven
- one root level README.md in the repo to describe the content
- another README.md file (use links) must describe expected application functionality (use at least a screen mockup / screenshot) 
- there must also be at least one user story
- there must be corresponding issues in Gitlab

== Requirements (3)

- you must reference the issues in the commits
- configure Maven  so that 
** application runs with  `mvn javafx:run` 
** test can run using `mvn test` 
- the build must report test coverage ( > 0% ) 
 
== Requirements (4)

- there must be at least something in each of the three architecture layers 
** domain logic, user interface (JavaFX-GUI) and persistence 
- project should be configured to open and run in eclipse che (link in README.md)

== Requirements (5)

- Readme.md file should include a section where requirements are specified - versions for Java, Maven, dependencies etc.
- Document usage of AI-tools. Why and what / why not
** (...groups-2024/gr24nn/gr24nn/docs/release1/ai-tools.md).
- any code taken / generated needs to reference the source in a comment

== Application description

- General description included in readme.md file
- user stories supported by additional design documents such as:
** conceptual model, 
** personas, 
** scenarios, 
** UI mockups, 
** UI protoypes
- User stories get broken down into issues and tasks
- Lead to a functional application




[background-color = "#124990"]
[color = "#fff6d5"]
== Build Tools

== Build tools (1)

[%step]
- Automate the process of building executable programs from source files
- Packaging binaries required for deployment / distribution
- Run automated tests

== Build tools (2)

[%step]
- Build automation is a necessity to enable CI/CD
- Remove the need to do redundant tasks
- Improve quality of the software
	** the software builds are prepared in a consistent and predictable manner
	** possible to have data to analyze issues and improve

== Make (1)

[%step]
- Designed by Stuart Feldman
- Released in 1976
- Uses makefiles to describe the actions required to produce the build
- Manages dependencies

[.smaller-40]
https://en.wikipedia.org/wiki/Make_(software)

== Make (2)

[%step]
- Has been rewriten a number of times
- Standard modern implementation is GNU Make
- Used in Linux and Mac OS

== Java world build tools

- Ant with Ivy
- Maven
- Gradle

== Apache ANT

[%step]
- modern build system
- released in 2000
- build files use XML
	** tends to get unmanageable even for small projects
- Apache Ivy for managing dependencies (added later)
	** download over network

[.smaller-40]
http://ant.apache.org

== Apache Maven (1)

[%step]
- released in 2004
- improves on ANT
- build files use also XML but the structure is radically different
- dependency management with automatic downloading over the network is available from release

[.smaller-40]
http://maven.apache.org

== Apache Maven (2)

[%step]
- hides complexity in build files through plugins 
- customizing is hard
- dependency management has issues with conflicting versions of same library  


[background-color = "#124990"]
[color = "#fff6d5"]
== Gradle


== Gradle (1)

[%step]
- released in 2012
- build scripts are written in a domain specific language based on Groovy
	**  Groovy ( http://www.groovy-lang.org/ )
- the build script is named `build.gradle`
- build steps are called "tasks"

[.smaller-40]
https://gradle.org


== Gradle (2)

[%step]
- easy to create own tasks
- uses plugins to hide complexity 
- applying plugins allows access to additional tasks 


== Gradle (3)

[.center-paragraph]
image::../images/lecture03/gradle-tree.png[width=700]

[.smaller-40]
https://guides.gradle.org/creating-new-gradle-builds/

[background-color = "#124990"]
[color = "#fff6d5"]
== More on Maven

== Maven (3)

* manages builds, dependencies, versions 
* configuration file is `pom.xml`
* has good IDE support
* central repository(ies) for dependencies

== Maven - pom.xml

* modelVersion (4.0.0) config file format version
* groupId - ID of group owning the project 
* artifactId - name of the final output
* version  - version of the created artifact

== Maven - pom.xml (cont.)

* dependencies - list of artifacts we depend upon
* packaging - e.g. .jar (Java archive)
* description 

https://maven.apache.org/pom.html#Quick_Overview

== Maven dependencies

* list of dependencies 
* each dependecy has specified
** groupId
** artifactId
** version (optional, good to have)  
** scope (default is `compile`)

== Maven build lifecycles

[.smaller-60]
* clean
* site
* default
** validate
** compile
** test
** package
** verify
** install
** deploy


== Maven plugins

* add functionality
* connected to a certain lifecycle phase
* provide additional goals

[background-color = "#124990"]
[color = "#fff6d5"]
== More on development environment


[background-color = "#124990"]
[color = "#fff6d5"]
== VSCode extensions



[background-color = "#124990"]
[color = "#fff6d5"]
== Eclipse Che


[background-color = "#124990"]
[color = "#fff6d5"]
== Agile Practices - Scrum

== User stories

[.smaller-80]
- short, simple descriptions for application features 
- formulated from the stand point of the user / customer
- template: 
** As a **< type of user >**, I want **< some goal >** so that **< some reason >**.
- they are not replacing design documents  / requirements specification
- they need to be developed into specific tasks and connected to constraints and other meaningful documentation. 

== Sprints
- meaningful iterations of comparable length
- they should have a clear goal

== Planning releases
- 3 deliverables - map to releases
- a release should produce a minimum viable product (MVP)
** a MVP is a version of an application with just enough features to be usable in getting feedback to guide the development process 


== Meetings
- regular stand-up meetings (synchronize and commit, remove hindrances)
- retrospectives (reflect on your group work)
- sprint reviews / demos (invite TA, prepare deliverables)

 


[background-color = "#124990"]
[color = "#fff6d5"]
== Summary


include::{includedir}footer.adoc[]