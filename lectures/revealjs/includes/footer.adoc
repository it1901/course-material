[subs="attributes"]
++++
 <div id="footer" class="footer">
 	<div style="display:table-row;">
     <span class="element" style="width:150px;">
     	<a href="https://www.ntnu.no" target="_blank">
     		<img 	id="footer-logo" class="footer-logo" 
     				src="images/template/logo_ntnu.png" 
     				alt="ntnu logo" height="28"/>
     	</a>
     </span>
     <span class="element" style="width:300px;">| IT1901 - {LECTURE_NO}</span>
     	<span class="element">| {LECTURE_TOPIC} </span>
     <span class="element">&nbsp;&nbsp;&nbsp;&nbsp;</span>
  </div>   
 </div>
 
 <div id="vertical-ntnu-name" class="vertical-ntnu-name">
 	<span class="helper"></span>
 	<img src="images/template/vertical-ntnu-name.png" alt="Norwegian University of Science and Technology" />
 </div>
 
 <script type="text/javascript">
     window.addEventListener("load", function() {
         revealDiv = document.querySelector("body div.reveal")
         footer = document.getElementById("footer");
         revealDiv.appendChild(footer);
         
         titleSlideDiv = document.querySelector("div.slides section.title")
         mainLogo = document.getElementById("main-logo");
         titleSlideDiv.prepend(mainLogo);
         
         vertName =  document.getElementById("vertical-ntnu-name");
         revealDiv.appendChild(vertName);
     } );
 </script>
++++