= Smidig praksis og verktøy
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Agile tools
:LECTURE_NO: 

include::{includedir}header.adoc[]


== Smidig praksis og verktøy

* fokuserer på bygging av produkt som kan prøves ut
* sentrert rundt prosesser for dette, og ikke på verktøy og teknikk
* men... forutsetter i praksis verktøystøtte
** smidighet krever automatisering
** verktøy smører prosessene

== Ulike typer verktøy

* oppfølging av den smidige prosessen
** oversikt over (ulike typer) utviklingsoppgaver
** planlegging av iterasjoner
** samarbeidsstøtte
* håndtering av kildekode
** distribuert utvikling
** endringshåndtering
** versjonskontroll

== Ulike typer verktøy forts.

* kontinuerlig integrasjon (CI)
** testing
** bygging av artifakter

== Historien om en utviklingsoppgave

Kunden ønsker å kunne importere et gammelt filformat. Dette funksjonsønsket deles opp i to utviklingsoppgaver, én for kjernefunksjonaliteten og én for brukergrensesnittet.

Cecilie starter med kjernen og skisserer først hvordan hun tenker å løse det teknisk vha. noen enkle klassediagram. Så defineres oppførselen til hver klasse i form av tester, og tomme klasser opprettes.

== Historien om ... forts.

Kristian kan starte på brukergrensesnittet til importfunksjonaliteten så snart klassedeklarasjonene er klare, han trenger ikke selve implementasjonskoden i starten.

Cecilie jobber seg så gjennom implementasjonen, og sjekker hele tiden at den er som tenkt ved å kjøre testene. Hun er ferdig i god tid før Kristian, og når han er det så... bare virker det! Til slutt oppsummerer de begge hva som ble gjort og gjør funksjonen tilgjengelig for kunden.

== Historien om ... og verktøy

Hva har dette å si for verktøystøtte?

* oppfølging av den smidige prosessen: 
** to utviklingsoppgaver knyttet til hhv. Cecilie og Kristian
** del av inneværende iterasjon
** dialog/diskusjonstråd knyttet til oppgaven 

== Historien om ... og verktøy forts.

* håndtering av kildekode:
** Cecilie må kunne dele sin kjernekode med Kristian, men holde den unna de andre
** det må komme frem at koden er knyttet til respektive utviklingsoppgaver
** den nye koden er knyttet til _neste_ versjon

== Historien om ... og verktøy forts.

* kontinuerlig integrasjon (CI):
** hyppig kjøring av testene
** ny versjon rulles raskt ut til kunden

== `gitlab.stud.idi...`

[.smaller-80]
* profesjonelt støtteverktøy for smidig utvikling
* IDI har egen installasjon (tas kanskje over av NTNU IT)
** innlogging med NTNU-navn og -passord
* hierarki av _grupper_, med _prosjekter_ (repo) under
** IT1901-emnet er en gruppe: https://gitlab.stud.idi.ntnu.no/it1901[it1901]
*** emnet har repo med læringsmateriale inkl. kode
** hver gruppe er egen _undergruppe_ (it1901-gr19xy)
*** gruppene opprettes av oss, så inviteres dere inn
*** dere oppretter nødvendige repo

== Oppgavesporing (issue tracking)

En _oppgave_ (issue) er arbeid som skal følges opp

* ny funksjon, forbedring, feilretting, konfigurasjon ...
* hver oppgave har en dialog/diskusjonstråd
* halvautomatisk knytning til _endringer_ (commits)

== Oppgavesporing forts.

Oppgavesporing er viktig for transparens

* kunder trenger innsyn i prosess
* teamet trenger å dele kunnskap
* løse og distribuerte prosjekter (f.eks. åpen kildekode) har ekstra behov
* støtter vurdering...

== Oppgavesporing forts.

[.smaller-80]
* oppgaver opprettes ifm. planlegging av iterasjon, f.eks. fra _brukerhistorier_, _funksjonsønsker_ eller _feilrapporter_
* oppgaver knyttes til
** _milepæl_ for iterasjon
** utviklinger som jobber med den
* _merkelapper_ (labels) kan angi _fasen_ en oppgave er i
** f.eks. planlagt, utvikling, testing, godkjent
** _oppgavetavler_ (issue boards) visualiserer fremdrift

== Oppgavetavle (issue board)

image::https://about.gitlab.com/images/blogimages/workflow.png[size=80%]

== Oppgavesporing forts.

* dialog/diskusjonstråd dokumenterer prosessen
** designidéer, avgjørelser, avhengigheter, ...
** knyttes til endringer (commits) gjennom oppgavenummer (#)
** oppsummerer hva som ble gjort

[.center-paragraph]
Viktig for transparens!

== Kildekodehåndtering

[.center-paragraph]
link:scm.html[Source code management (SCM)]

== Kontinuerlig integrasjon

[.center-paragraph]
link:ci.html[Continuous Integration (CI)]


include::{includedir}footer.adoc[]