= Code quality 
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Code quality 
:LECTURE_NO: 10th Lecture

include::{includedir}header.adoc[]

[.smaller-80][.center-paragraph]
IT1901 Fall 2021 - {LECTURE_NO}


[background-color = "#124990"]
[color = "#fff6d5"]
== Code quality

== Code quality

- automated tests
- coding standards and consistent formatting
- check for higher level types of errors (bug patterns)

== Automated tests

- decrease the effort for running and re-running tests
- there are several types metrics that can be used to measure test coverage

== JaCoCo

- Java Code Coverage (JaCoCo)
- popular tool for measuring code coverage in Java projects
- measures several metrics  

== JaCoCo metrics

- Instructions 
- Branches (exception handling is excluded)
- Cyclomatic Complexity - "According to  McCabe1996 cyclomatic complexity is the minimum number of paths that can, in (linear) combination, generate all possible paths through a method."
- Lines
- Methods
- Classes

[.smaller-40]
https://www.jacoco.org/jacoco/trunk/doc/counters.html

== Tools for automatic code checking

- Checkstyle (https://checkstyle.sourceforge.io/index.html) 
- Spotbugs (https://spotbugs.readthedocs.io/en/latest/index.html)
- Pmd (https://pmd.github.io)
- Sonarcube (https://www.sonarqube.org/)
- ... 

== Checkstyle

- static analysis
- finds errors like
** references to variables with undefined value
** variables that are never used
** syntax and coding standards violations
** dead code blocks 

== Checkstyle (2)

- naming conventions
- line length
- whitespace
- Javadoc comments
- annotations
 
== Checkstyle (3)

- can be configured to use a certain set of checks
- extendable - one can write own checks
- plugins for IDEs
 
[background-color = "#124990"]
[color = "#fff6d5"]
== Checkstyle examples 

 
== Spotbugs

- checks bytecode and not the source
- looks for "bug patterns"
- example:

```
An apparent infinite loop (IL_INFINITE_LOOP)
This loop doesn't seem to have a way to terminate (other than by perhaps throwing an exception).
```

== Spotbugs (2)

- extendable
- plugins for Maven Gradle Ant
- Eclipse IDE integration 

[background-color = "#124990"]
[color = "#fff6d5"]
== Spotbugs examples


include::{includedir}footer.adoc[]