= Modular software 
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Modular software 
:LECTURE_NO: 9th Lecture

include::{includedir}header.adoc[]

[.smaller-80][.center-paragraph]
IT1901 Fall 2023 - {LECTURE_NO}


== Overview
- Group Assignment 2 (GA2)
- Architectural styles
- Modular projects with Maven and Java
- More on Eclipse Che
- VSCode devcontainers
- Summary


[background-color = "#124990"]
[color = "#fff6d5"]
== Group Assignment 2 (GA2)


== GA2 - modularisering

* kodingsprosjektet skal deles opp i egnede moduler med avhengigheter
* bygging styres (fortsatt) av maven og hver modul har hensiktmessig konfigurasjon
* maven-moduler er et krav, mens Java-moduler er ikke (men i utgangspunktet anbefalt)
* mer om modularisering senere

[.smaller-80]
== GA2 - arkitektur

* kjernelogikken (domenelaget) og ui (brukergrensesnittlaget) skal skilles fra hverandre
* persistens med JSON vha. egnet bibliotek, f.eks. Jackson
** definere/dokumentere (?) filformat for brukerdata og evt. innstillinger
* reflektere over og velge mellom dokumentmetafor (desktop) og implisitt lagring (app)
* alle tre lagene spiller sammen

== GA2 - kodekvalitet

* tester skal skrives for alle modulene 
* testdekningsgrad og (annen) kodekvalitet skal sjekkes av egne maven-tillegg (f.eks. **jacoco**, **checkstyle** og **spotbugs**)
* rimelig god testdekning av alle lagene

== GA2 - dokumentasjon

* generell dokumentasjon må holdes oppdatert
* arkitektur med minst et diagram (bruk PlantUML) i tillegg til teksten i readme
* valg knyttet til arbeidsvaner, arbeidsflyt, testing, bruk av verktøy for sjekking av kodekvalitet (f.eks. innstillinger)

== GA2 - arbeidsvaner

* kodingsoppgaver skal være utgangspunktet for alt arbeid
* greiner (branch) samler arbeid for hver kodingsoppgave (?)
** se alternativet **trunk based development**
* bruk milepæl (milestones) knyttet til innleveringen
* dere jobber i par og bytter på å kode
* "produktiv" kode og tester må holde tritt

== GA2 - lær (i smidig ånd)

* hva virker bra og ikke så bra
* hva er en god oppdeling i og fordeling av arbeidsoppgaver
* ikke lur dere selv eller andre

[.smaller-80]
== GA2 - leveranse
* prosjektet må være eclipse che-klart med eclipse che lenke i README-fila
* maven skal brukes til kjøring, testing og sjekk av kodekvalitet
* kode må ligge i master-greina av standard kodelager for gruppa
* på Blackboard leveres en enkel tekst om at kodelageret er klart for vurdering



[background-color = "#124990"]
[color = "#fff6d5"]
== Architectural styles

== Architectural styles

An architectural style is a conceptual specification of how a certain
system will be structured and function. 

== Architectural styles

- monolithic applications
- 3-tier applications
- REST

== layers vs. tiers

- sometimes the terms are used interchangeably
- there is a difference
** layers - logical separation
** tiers - physical separation

[background-color = "#124990"]
[color = "#fff6d5"]
== monolithic applications

== monolithic applications (1)

- All the functionality is packed in a single software unit
** ui
** logic
** data
- designed without modularity
- also called single-tiered application

== !

image::../images/lecture12/1tier.png[canvas, size=contain]

[background-color = "#124990"]
[color = "#fff6d5"]
== 3-tier applications

== !

image::../images/lecture12/3tier.png[canvas, size=contain]

== 3-tier applications (2)

- application where the various functions are separated
** presentation tier (UI / frontend)
** logic tier (business tier)
** data tier (data storage + data access)
- also called 3-layer
- application is modular 

== 3-tier applications (3)

- ability to update / replace just parts of the application
- ability to independently scale the layers
- ability to re-use layers

[background-color = "#124990"]
[color = "#fff6d5"]
== REST

[.smaller-80]
== REST
- Representational state transfer (REST)  
- architectural style involving use of Web Services 
- set of constraints are applied 
** client server
** statelessness (no client context is stored on the server side)
** cacheability (responses state if they can be cached or not)
** uniform interface
** layered system (adding layers like proxy or load balancer)


== REST (cont.)

- Web services that implement REST are called RESTful APIs
- a base URI, example: https://gitlab.stud.idi.ntnu.no/api/v4
- standard HTTP methods (e.g., GET, POST, PUT, PATCH and DELETE);
- data formats for requests and responses (json, xml, etc)

== !

image::../images/lecture12/itodo.png[canvas, size=contain]

== !

image::../images/lecture12/mtodo.png[canvas, size=contain]


== !

image::../images/lecture12/rtodo.png[canvas, size=contain]


[background-color = "#124990"]
[color = "#fff6d5"]
== Modularity

== Modularity

- means for splitting a large problems into parts
** distribute and isolate work
** distribute artifacts for use and reuse
- more or less supported by tools and languages
** maven - modular build
** java - strong encapsulation

[background-color = "#124990"]
[color = "#fff6d5"]
== Modularity with maven

== Modular projects with Maven

- main "parent" module with common configuration
- sub-modules with dependencies
** libraries (and plugins) are modules
** own modules follow architecture
- manages build tasks
** sequenced according to dependencies
** execute in appropriate context e.g. classpath

== Modular projects with Maven

- use of inheritance
- parent `pom` has configuration that is inherited by modules' `pom`
- the descendent `pom` can override inherited configuration elements
- descendant `pom` inherits most elements with the exception of things like artifactid, name which are used to identify the parent `pom`

== Modular projects with Maven (2)

- reduce complexity and size of individual build files
- reliably use consistent versions for dependencies and plugins 

== Parent pom example

[.smaller-40]
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>it1901.todolist</groupId>
    <artifactId>parent</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
               ...
            </dependency>
            ...
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
      				...
                </plugin>
                ...
            </plugins>
        </pluginManagement>
    </build>

    <modules>
        <module>core</module>
        <module>fxui</module>
    </modules>
</project>

```

== Descendant pom example

[.smaller-40]
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>it1901.todolist</groupId>
        <artifactId>parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>

    <artifactId>fxui</artifactId>

    <dependencies>
		<dependency>
			...
		</dependency>
		...
    </dependencies>

    <build>
        <plugins>
            <plugin>
                ...
            </plugin>
            ...
        </plugins>
    </build>
</project>

```

[background-color = "#124990"]
[color = "#fff6d5"]
== Modularity with Java

== Java modules

[.smaller-80]
* corresponds to a set of packages
* strong encapsulation
** explicitly export packages
** explicitly state dependencies
** also enforced during runtime
* advantages
** avoid relying on internals
** build lighter apps
* disadvantages...

== core - module-info.java

```java
module todolist.core {
    requires transitive com.fasterxml.jackson.databind;

    exports todolist.core;
    exports todolist.json;
}
```

== core - https://gitlab.stud.idi.ntnu.no/it1901/todo-list/-/blob/master/todolist/core/src/main/java/module-info.java[module-info.java]

* module name
** how to get (guess) name of libraries 
* **requires** - what this module needs
** **transitive** - others will need this too
** **exports** - what others may use

== fxui - https://gitlab.stud.idi.ntnu.no/it1901/todo-list/-/blob/master/todolist/fxui/src/main/java/module-info.java[module-info.java]

```java
module todolist.ui {
    requires com.fasterxml.jackson.databind;

    requires java.net.http;

    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;

    requires todolist.core;
    requires fxutil;

    opens todolist.ui to javafx.graphics, javafx.fxml;
}
```

== fxui - module-info.java

* **opens** ... *to* - allow getting runtime info about classes using so-called _reflection_
** fxml
** REST frameworks
** test frameworks
* **--add-opens** command line option
** support unmodularized mode

[background-color = "#124990"]
[color = "#fff6d5"]
== More on Eclipse Che

== Eclipse Che IT1901

[.smaller-80]
- https://che.stud.ntnu.no
- current limitations 
 ** [.line-through]#container UI needs to be started manually#
 ** UI might lock (screensaver) - [.line-through]#we need to unlock using terminal commands# 
 ** every user is limited to 1 running workspace
 ** [.line-through]#stoping a workspace loses changes#  (you should always push your changes before closing)

[background-color = "#124990"]
[color = "#fff6d5"]
== VSCode Devcontainers

== VSCode Devcontainers

- VSCode extension "Dev Containers"
** related extension "Docker"
** needs "Docker Desktop" installed locally
- allow developing inside a Docker container
- ensure local development with a consistent and reproducible environment

== devcontainer.json
```json
{
    "image":"adrianstoica/it1901:latest",
    "forwardPorts": [6080,8080,5901],
    "postStartCommand": "nohup bash -c '~/.vnc/startup  &'",
    "customizations":{
        "vscode": {
            "extensions" : [
                "vscjava.vscode-java-pack",
                "vscjava.vscode-java-debug",
                "vscjava.vscode-maven",
                "vscjava.vscode-java-dependency",
                "vscjava.vscode-java-test",
                "redhat.java",
                "yzhang.markdown-all-in-one",
                "shd101wyy.markdown-preview-enhanced",
                "DavidAnson.vscode-markdownlint",
                "jebbs.plantuml",
                "ms-vscode.live-server",
                "vsls-contrib.codetour",
                "ritwickdey.liveserver"
            ]
        }
    }
}

```


[background-color = "#124990"]
[color = "#fff6d5"]
== Summary

include::{includedir}footer.adoc[]