= Unit testing 
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Unit testing 
:LECTURE_NO: 8th Lecture

include::{includedir}header.adoc[]

[.smaller-80][.center-paragraph]
IT1901 Fall 2021 - {LECTURE_NO}


[background-color = "#124990"]
[color = "#fff6d5"]
== Testing

== Testing

[%step]
- is an important part of software development
- a way to ensure software quality
- automated testing allows to develop new features with a minimal effort to check if the software still works as expected
- testing frameworks  

== Testing (2)

[%step]
- design 
- implement
- write automated tests
- run tests
- we do not test just for know, we write tests to keep running them during project life cycle 

== Testing (3)

[%step]
- design tests
- implement the test
- provide inputs
- run the tests
- provide expected outputs
- check if the result  we get matches what we expect
- produce a manageable output that the developer can consult 

== Testing (3)

- design tests
- implement the test
- provide inputs
- *run the tests*
- provide expected outputs
- *check if the result  we get matches what we expect*
- *produce a manageable output that the developer can consult* 


[background-color = "#124990"]
[color = "#fff6d5"]
== JUnit


== JUnit

- Is a Java unit testing framework.
- provides the means to automate test
- allows to eliminate redundant testing tasks  


== JUnit 4

``` java
import org.junit.*;

public class FoobarTest {
    @BeforeClass
    public static void setUpClass() throws Exception {
        // Code executed before the first test method
    }

    @Before
    public void setUp() throws Exception {
        // Code executed before each test
    }
 
    @Test
    public void testOneThing() {
        // Code that tests one thing
    }

    @Test
    public void testAnotherThing() {
        // Code that tests another thing
    }

    @Test
    public void testSomethingElse() {
        // Code that tests something else
    }

    @After
    public void tearDown() throws Exception {
        // Code executed after each test 
    }
 
    @AfterClass
    public static void tearDownClass() throws Exception {
        // Code executed after the last test method 
    }
}
```

[.smaller-40]
https://en.wikipedia.org/wiki/JUnit

== JUnit 5 

``` java
import org.junit.jupiter.api.*;

public class FoobarTest {
    @BeforeAll
    public static void setUpClass() throws Exception {
        // Code executed before the first test method
    }

    @BeforeEach
    public void setUp() throws Exception {
        // Code executed before each test
    }
 
    @Test
    public void testOneThing() {
        // Code that tests one thing
    }

    @Test
    public void testAnotherThing() {
        // Code that tests another thing
    }

    @Test
    public void testSomethingElse() {
        // Code that tests something else
    }

    @AfterEach
    public void tearDown() throws Exception {
        // Code executed after each test 
    }
 
    @AfterAll
    public static void tearDownClass() throws Exception {
        // Code executed after the last test method 
    }
}
```

== JUnit 4 vs JUnit 5

[.smaller-60]
[%header,cols=3*] 
|===
|Element
|JUnit 4
|JUnit 5


|package
|org.junit.*
|org.junit.jupiter.api.*

|code before first test
|@BeforeClass
|@BeforeAll

|code before each test
|@Before
|@BeforeEach

|test
|@Test
|@Test

|code after each test
|@After
|@AfterEach

|code after last test
|@AfterClass
|@AfterAll



|===

== JUnit 4 vs JUnit 5 (2)

[.smaller-60]
[%header,cols=3*] 
|===
|Element
|JUnit 4
|JUnit 5


|ignoring test
|@Ignore
|@Disabled

|assertions *statically* accessed with
|`Assert.assert`...
|`Assertions.assert`...

|assertion messsage position
|in the beginning of argument list
|in the end of argument list   

|===

== Expecting exceptions (1)

``` java
@Test
void testAssertingExpectedException() {
	Assertions.assertThrows(<ExpectedException.class>, () -> {
	    <code expected to throw exception>;
		});
}
```

== Expecting exceptions (2)

- if the code throws
** the exception class that is expected the the test will PASS
** a descendent class exception that is expected the the test will PASS
** a different exception type the test will FAIL
** throws no exception the test will FAIL


[background-color = "#124990"]
[color = "#fff6d5"]
== Testing in todo-list example


include::{includedir}footer.adoc[]