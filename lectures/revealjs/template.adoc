= Title here
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Topic
:LECTURE_NO: Nth lecture

include::{includedir}header.adoc[]


[.smaller-80][.center-paragraph]
IT1901 Fall 2021 - {LECTURE_NO}

[background-color = "#124990"]
[color = "#fff6d5"]
== Section slide

== Normal slide

- Blah
- Foo
- Bar

[.grid-left-right-50-50]
== 2 column slide 

[.area-left]
- blah blah cloud
** foo
** bar
** baz

[.area-right]
- blah blah cloud
** right foo
** right bar
** righ baz


include::{includedir}footer.adoc[]