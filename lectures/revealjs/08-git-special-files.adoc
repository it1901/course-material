= Git Special Files
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Git Special Files 
:LECTURE_NO: 8th Lecture

include::{includedir}header.adoc[]

[.smaller-80][.center-paragraph]
IT1901 Fall 2023 - {LECTURE_NO}


[background-color = "#124990"]
[color = "#fff6d5"]
== Git Special Files

== Overview

- .gitignore
- .mailmap 
- .gitkeep 
- .gitattributes 

== .gitignore

- configure files to ignore 
- https://git-scm.com/docs/gitignore
- https://gitignore.io

== .mailmap

- map author / committer names and emails
- https://git-scm.com/docs/gitmailmap


== .mailmap

``` 
# Hallvard
Hallvard Trætteberg <hal@ntnu.no> Hallvard Trætteberg <hal@ntnu.no>       
Hallvard Trætteberg <hal@ntnu.no> Hallvard Traetteberg <hal@ntnu.no>
Hallvard Trætteberg <hal@ntnu.no> hal <hal@ntnu.no>
# Adrian
George Adrian Stoica <stoica@ntnu.no> George Adrian Stoica <stoica@ntnu.no>
George Adrian Stoica <stoica@ntnu.no> Adrian Stoica <stoica@ntnu.no>

```

== .gitkeep

- keep in the repository empty folders
- git keeps track of folders that have files 
- not really a git special file 
- widely used convention
- typically we remove the file when we add other files to the folder 

== .gitattributes

- https://www.git-scm.com/docs/gitattributes
- control git attributes per path
- for example configure how EOL is handled

== .gitattributes

```
# Set the default behavior, in case people don't have core.autocrlf set.
* text=auto

# Set linux endings for . files
.bashrc text eol=lf
.xinitrc text eol=lf

# Explicitly declare text files you want to always be normalized and converted
# to native line endings on checkout.
*.c text
*.h text

# Declare files that will always have CRLF line endings on checkout.
*.sln text eol=crlf

# Denote all files that are truly binary and should not be modified.
*.png binary
*.jpg binary

```
[smaller-40]
https://docs.github.com/en/get-started/getting-started-with-git/configuring-git-to-handle-line-endings


[background-color = "#124990"]
[color = "#fff6d5"]
== Summary


include::{includedir}footer.adoc[]