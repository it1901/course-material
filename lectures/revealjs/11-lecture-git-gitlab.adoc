= Git and GitLab 
:customcss: slides.css
:icons: font
:includedir: includes/
:LECTURE_TOPIC: Git and GitLab 
:LECTURE_NO: 11th Lecture

include::{includedir}header.adoc[]

[.smaller-80][.center-paragraph]
IT1901 Fall 2021 - {LECTURE_NO}

[.smaller-80]
== Overview
- Git
** basic workflow recap
** working with branches
** code review
- GitLab
** Issues
** Milestones
** Merge requests
** Issues / MR Templates

== Basic workflow

- work on main branch (master)
- could be usable for individual / very small teams 
- prone to conflicts 

[.grid-left-right-50-50]
== Typical sequence (basic)

[.area-left]
- working with a shared project
** `git clone`
** `git pull`

[.area-right]
** `git status` 
** `git add ...`
** `git commit ...`
** `git push`

== Conflicts (1)

** when same files (lines) are changed by different devs
** automatic merge is not possible 
** we need to solve that to be able to push our changes 

== Conflicts (2) - Setup

- `git config merge.tool vimdiff`
- `git config merge.conflictstyle diff3`
- `git config mergetool.prompt false`

== Conflicts (3) - Solving

- `git mergetool`
- `:wqa` save and exit from vi
- `git commit -m "message"`
- `git clean` remove files (might remove other untracked files)

== Conflicts (4)

- one can plan ahead what issues should be worked on in parallel to minimize the chance of conflicts
- when working on a feature branch, keeping up to date with relevant branches (master or other branch we depend upon) can reduce the risk of getting conflicts


== Improved workflow 

- keep master only for finished work
- uses branches for development work
- when a feature branch is done it is merged into master

[.grid-left-right-50-50]
== Typical sequence (branches)

[.area-left]
** `git clone`
** `git pull` 
** `git checkout -b <branch_name>` 

[.area-right]
** `git status` + `git add ...` + `git commit ...` 
** `git checkout master` 
** `git merge <branch_name>` 
** `git push`


== Improved workflow (code review) 

- keep master only for finished work
- uses branches for development work
- when a feature branch is done a merge request is created
- code review takes place and more commits happen to address the comments
- the branch is finally merged into master 

== Gitlab specific push options

- push options

```bash 
-o merge_request.create 
-o merge_request.target=my-target-branch 
-o merge_request.label="Label with spaces"
```

== Advanced workflow (git flow)
- https://nvie.com/posts/a-successful-git-branching-model/
- uses 2 long lived main branches - `master` and `develop`
- shorter lived branches for `features`, `releases` and `hotfixes`

== Trunk-based development
- does not use shared long lived branches
- commits go to trunk (main / master) branch
- https://trunkbaseddevelopment.com

== Gitlab for agile

* Issues
* Milestones
* Task lists
* Labels
* Boards
* Quick actions

== Using templates

* Issue templates
* Merge request templates


[background-color = "#124990"]
[color = "#fff6d5"]
== Summary

include::{includedir}footer.adoc[]