# Materiale laget med asciidoctor

Mappa **asciidoctor** inneholder tekstlig materiale, mens **revealjs**-mappa
inneholder lysarkene. 

## Hvordan bygge

Bygging av både dokumentasjon og lysark skjer med gradle.

Bruk `gradle asciidoctor` for å generere HTML for det tekstlige og `gradle asciidoctorRevealJs` for lysarkene.
Konvertering kan også gjøres med den generelle `gradle build`.
